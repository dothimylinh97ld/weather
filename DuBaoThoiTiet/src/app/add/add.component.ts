import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';
import { IWeather } from '../interfaces/weather.interface';
import { WeatherService } from '../services/weather.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.scss']
})
export class AddComponent implements OnInit {

  checked = false;
  change = false;
  city:string;

  constructor(private router: Router, private userService: UserService, private weatherService: WeatherService,
    private toast : ToastrService) { }


  ngOnInit() {
  }
  onChange(e) {
    this.checked = !this.checked;
  }
  onNext(e) {
    this.change = !this.change;
  }
  onHome() {
    this.router.navigate([`/home`]);
  }
  isHome() {
    this.router.navigate([`/home`]);
  }

  onLogin(username, password) {
    const user: User = {
      email: username,
      hash_password: password
    }
    this.userService.onLogin(user).subscribe((response) => {
      if (response) {
        localStorage.setItem('user', JSON.stringify(response));
        this.router.navigate([`/admin`]);
      }
    }, err => {
      if(err){
        alert(err.error.message)
      }
    })
  }
  onClickBack(){
    this.router.navigate([`/admin`]);
  }
  onThem(thanhpho, tempnow, tempmax, tempmin, state, uv, windspeed, doam,
    day1, temp1,state1,state2, state3, state4,
    day2, day3, day4,temp2, temp3, temp4 ){
      
      const weather: IWeather = {
        name:thanhpho,
        tempmax:tempmax,
        tempmin:tempmin,
        tempnow:tempnow,
        statenow:state,
        humid:doam,
        uv:uv,
        description: 'dsa',
        time:'das',
        winspeed:windspeed,
        day1:day1,temp1:temp1,state1:state1,
        day2:day2,temp2:temp2,state2:state2,
        day3:day3,temp3:temp3,state3:state3,
        day4:day4,temp4:temp4,state4:state4
      };
      this.weatherService.createWeather(weather).subscribe(res=>{
        if(res){
          this.toast.success("Thêm thành công");
          this.router.navigate(['admin']);
        }
      }, err => {
      if(err.status === 404){
        console.log(err.error.message);
        alert(err.error.message);
      }
      console.log(err);
      
    })
  }
}