import { Component, OnInit, Inject } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { WeatherService } from '../services/weather.service';
import { IWeather } from '../interfaces/weather.interface';
import { Router } from '@angular/router';

import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '100vh',
        width: '170px',
        opacity: 1,
      })),
      state('closed', style({
        height: '100vh',
        width: '0px',
        opacity: 0,
      })),
    ]),
  ],
})
export class AdminComponent implements OnInit {

  public textSearch: String = '';
  public results: IWeather[] = [];
  checked = false;
  disabled = false;
  isOpen = false;
  states: IWeather[] = [];
  user: any;
  search = '';
  slideConfig = { 'slidesToShow': 3, 'slidesToScroll': 3, 'dots': true };
  constructor(private weatherService: WeatherService, 
              private router: Router,
              private confirmationDialogService: ConfirmationDialogService,
              private toast: ToastrService
              ) { }

  ngOnInit() {
    this.fetchData();
    this.user = JSON.parse(localStorage.getItem('user'));
    
  }
  onChange(e) {
    this.checked = !this.checked;
  }

  fetchData(){
    this.weatherService.getWeathers().subscribe((data: IWeather[]) => {
      this.states = data;
    });
  }
  toggle() {
    this.isOpen = !this.isOpen;
  }

  onSearch(keyword) {
    if (keyword !== '') {
      this.weatherService.findByCityName(keyword).subscribe(data => {
        this.states = data;
      });
    }else{
      this.fetchData();
    }

  }
  onLogin(){
    this.router.navigate(['/login']);
  }
  onEdit(id){
    this.router.navigate([`/edit/${id}`]);
  }
  onThem(){
    this.router.navigate([`/add`]);
  }
  public openConfirmationDialog(id) {
    this.confirmationDialogService.confirm('Xác nhận', 'Xóa thành phố ?')
    .then((confirmed) => {
      if(confirmed === true){
        this.weatherService.deleteWeather(id).subscribe(_=> {
              this.weatherService.getWeathers().subscribe(data=>{
                this.states = data;
                console.log(id);
              })
            })
        this.toast.success('Successfully');
      }
    })
    .catch(() => console.log('User dismissed the dialog'));
  }
  
}
