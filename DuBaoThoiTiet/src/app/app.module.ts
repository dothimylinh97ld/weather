import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { SlickModule } from 'ngx-slick';

import { SlideToggleModule } from 'ngx-slide-toggle';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DetailsComponent } from './details/details.component';
import { HttpClientModule } from '@angular/common/http';
import { StatePipe } from './pipes/state.pipe';
import { NotePile } from './pipes/note.pipe';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { AdminComponent } from './admin/admin.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from './confirmation-dialog/confirmation-dialog.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { EditComponent } from './edit/edit.component';
import { AddComponent } from './add/add.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DetailsComponent,
    StatePipe,
    NotePile,
    LoginComponent,
    SignupComponent,
    AdminComponent,
    ConfirmationDialogComponent,
    EditComponent,
    AddComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
   MatCardModule,
    SlideToggleModule,
    SlickModule.forRoot(),
    HttpClientModule,
    ToastrModule.forRoot(),
    NgbModule,
    NgbModule.forRoot(),
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [ConfirmationDialogService],
  bootstrap: [AppComponent],
  entryComponents: [ ConfirmationDialogComponent ],
})
export class AppModule { }
