import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../services/weather.service';
import { ActivatedRoute, Router } from '@angular/router';
import { IWeather } from '../interfaces/weather.interface';
import { trigger, state, style } from '@angular/animations';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '100vh',
        width: '170px',
        opacity: 1,
        //backgroundColor: 'white'
      })),
      state('closed', style({
        height: '100vh',
        width: '0px',
        opacity: 0,
        //backgroundColor: 'white'
      })),
      // transition('open => closed', [
      //   animate('0.1s')
      // ]),
      // transition('closed => open', [
      //   animate('1s')
      // ]),
    ]),
  ],
})
export class DetailsComponent implements OnInit {

  id: string;
  state: IWeather;
  states: IWeather[] = [];
  checked = false;
  disabled = false;
  isOpen = false;
  slideConfig = { 'slidesToShow': 3, 'slidesToScroll': 3, 'dots': true };
  dataInfoWeather:any[]=[];
  constructor(private weatherService: WeatherService, 
    private activatedRoute: ActivatedRoute, 
    private router: Router)
   { }

  ngOnInit() {
    
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
     
      this.weatherService.getWeather(this.id).subscribe((data: IWeather) => {
        this.state = data;
      });
    });
    this.weatherService.getWeathers().subscribe((data: IWeather[]) => {
      this.states = data;
      console.log(data);
    });
  }
  toggle() {
    this.isOpen = !this.isOpen;
  }
  onChange(e) {
    this.checked = !this.checked;
  }
  onClickBack(){
    this.router.navigate([`/home`]);
  }
  
}
