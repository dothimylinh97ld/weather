import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';
import { WeatherService } from '../services/weather.service';
import { IWeather } from '../interfaces/weather.interface';
import { ToastrModule, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  change = false;
  checked = false;
  id: string;
  state: IWeather;
  constructor(private router: Router,private toastr:ToastrService, private userService: UserService,private activatedRoute: ActivatedRoute, private weatherService: WeatherService) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.weatherService.getWeather(this.id).subscribe((data: IWeather) => {
        this.state = data;
      });
    });
  }
  onNext(e) {
    this.change = !this.change;
  }
  onChange(e) {
    this.checked = !this.checked;
  }
  onHome() {
    this.router.navigate([`/home`]);
  }
  isHome() {
    this.router.navigate([`/home`]);
  }

  
  onClickBack(){
    this.router.navigate([`/admin`]);
  }

  onEdit(id, thanhpho, nhietdohientai, 
    nhietdocaonhat, nhietdothapnhat, doam, 
    trangthai, uv, tocdogio, day1, temp1, state1,
    day2, temp2, state2,
    day3, temp3, state3,
    day4, temp4, state4 ){
      const weather: IWeather = {
        name:thanhpho,
        tempmax:nhietdocaonhat,
        tempmin:nhietdothapnhat,
        tempnow:nhietdohientai,
        statenow:trangthai,
        humid:doam,
        time:'',
        description:'',
        uv:uv,
        winspeed:tocdogio,
        day1:day1,
        temp1:temp1,
        state1:state1,
        day2:day2,
        temp2:temp2,
        state2:state2,
        day3:day3,
        temp3:temp3,
        state3:state3,
        day4:day4,
        temp4:temp4,
        state4:state4
      };
      this.weatherService.updateWeather(weather, id).subscribe(data=>{
        this.weatherService.getWeather(id).subscribe(dt=>{
          this.state = dt;
          this.toastr.success('Cập nhật thành công!');  
          this.router.navigate([`/admin`]);
        })     
      })
  }

}
