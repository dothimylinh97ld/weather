import { Component, OnInit } from '@angular/core';
import { trigger, style, state, transition, animate } from '@angular/animations';
import { WeatherService } from '../services/weather.service';
import { IWeather } from '../interfaces/weather.interface';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '100vh',
        width: '170px',
        opacity: 1,
      })),
      state('closed', style({
        height: '100vh',
        width: '0px',
        opacity: 0,
      })),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  public textSearch: String = '';
  public results: IWeather[] = [];
  checked = false;
  disabled = false;
  isOpen = false;
  states: IWeather[] = [];
  user: any;
  search = '';
  slideConfig = { 'slidesToShow': 3, 'slidesToScroll': 3, 'dots': true };

  constructor(private weatherService: WeatherService, private router: Router) { }
  ngOnInit() {
    this.fetchData();
  }
  fetchData(){
    this.weatherService.getWeathers().subscribe((data: IWeather[]) => {
      this.states = data;
      console.log(data);
    });
  }
  toggle() {
    this.isOpen = !this.isOpen;
  }
  onChange(e) {
    this.checked = !this.checked;
  }
  onSearch(keyword) {
    if (keyword !== '') {
      this.weatherService.findByCityName(keyword).subscribe(data => {
        this.states = data;
      });
    }else{
      this.fetchData();
    }
  }
  onSignup(){
    this.router.navigate(['/signup']);
  }
  onLogin(){
    this.router.navigate(['/login']);
  }
  onDetails(id) {
    this.router.navigate([`/details/${id}`]);
  }
}
