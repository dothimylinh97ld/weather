export interface IWeather {
    _id?: number;
    name: string;
    description:string;
    time:string;
    tempmax: string;
    tempmin: string;
    tempnow: string; // nhiet do hien tai
    humid: string; // do am
    statenow: string // trang thai hien tai
    winspeed: string; // toc do gio
    uv: string; // tia UV
    day1?:string;
    temp1?:string;
    state1?:string;
    day2?:string;
    temp2?:string;
    state2?:string;
    day3?:string;
    temp3?:string;
    state3?:string;
    day4?:string;
    temp4?:string;
    state4?:string;
    // day5:string;
    // temp5:string;
    // state5:string;
    // day6:string;
    // temp6:string;
    // state6:string;
}