import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  checked = false;

  constructor(private router: Router, private userService: UserService) { }


  ngOnInit() {
  }
  onChange(e) {
    this.checked = !this.checked;
  }
  onHome() {
    this.router.navigate([`/home`]);
  }
  isHome() {
    this.router.navigate([`/home`]);
  }

  onLogin(username, password) {
    const user: User = {
      email: username,
      hash_password: password
    }
    this.userService.onLogin(user).subscribe((response) => {
      console.log(response);
      
      if (response) {
        localStorage.setItem('user', JSON.stringify(response));
        this.router.navigate([`/admin`]);
      }
    }, err => {
      if(err){
        alert("Vui lòng kiểm tra lại thông tin đăng nhập")
      }
    })
  }
}
