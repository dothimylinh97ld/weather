import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'note'
})
export class NotePile implements PipeTransform {
    transform(status: any): any {
        let note;
        switch (status) {
            case "Nắng lớn":
                note = 'Nhớ đem theo nón che và kem chống nắng';
                break;
            case "Có mưa":
                note = 'Nhớ đem theo dù và áo ấm';
                break;
            case "Trời bão":
                note = 'Bạn nên ở nhà trong tình trạng thời tiết xấu như thế này';
                break;
            case "Mát mẻ":
                note = 'Trời đẹp, ra ngoài hít thở không khí trong lành thôi!'
                break;
            default:
                note = '';
                break;
        }
        return note;
    }
}