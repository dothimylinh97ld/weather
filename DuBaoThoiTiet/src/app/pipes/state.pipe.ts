import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'state'
})
export class StatePipe implements PipeTransform {
    transform(status: any): any {
        let value;
        switch (status) {
            case "Nắng lớn":
                value = 'Nắng lớn';
                break;
            case "Có mưa":
                value = 'Có mưa';
                break;
            case "Trời bão":
                value = 'Trời bão';
                break;
            case "Mát mẻ":
                value = 'Mát mẻ';
                break;
            default:
                value = '';
                break;
        }
        return value;
    }
}