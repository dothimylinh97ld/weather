import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../interfaces/user.interface';
import { Observable } from 'rxjs';

@Injectable({
    "providedIn": "root" // ko can import trong app module
})
export class UserService {
    constructor(private http: HttpClient){}
    Port = 'http://localhost';

    onLogin(user: User): Observable<User>{
        return this.http.post<User>(`${this.Port}:5000/api/user/sign`,user);
    }

    onSignUp(user: User): Observable<User>{
        return this.http.post<User>(`${this.Port}:5000/api/user/create`,user);
    }
}