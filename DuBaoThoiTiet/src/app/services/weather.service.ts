import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators'
import { IWeather } from '../interfaces/weather.interface';

@Injectable({
    providedIn: "root"
})

export class WeatherService {
    weathers: IWeather[] = [];
    idWheather = new BehaviorSubject<string>(null);
    wthers = new BehaviorSubject<IWeather[]>([]);
    Port = 'http://localhost';

    constructor(private http: HttpClient) { }


    createWeather(wt:IWeather):Observable<IWeather>{
        return this.http.post<IWeather>(`${this.Port}:5000/api/Postweather`,wt);
    }
    updateWeather(weather: IWeather, id: string):Observable<IWeather>{
        return this.http.put<IWeather>(`${this.Port}:5000/api/weather/${id}`,weather);
    }
    getWeathers(): Observable<IWeather[]> {
        return this.http.get<IWeather[]>(`${this.Port}:5000/api/weather`);
    }
    getWeather(id: string): Observable<IWeather> {
        return this.http.get<IWeather>(`${this.Port}:5000/api/weather/${id}`);
    }
    deleteWeather(id: string): Observable<IWeather> {
        return this.http.delete<IWeather>(`${this.Port}:5000/api/Deleteweather/${id}`);
    }
    setidWheather(id: string) {
        this.idWheather.next(id);
    }
    getIdWheather() {
        return this.idWheather.asObservable();
    }
    setWheather(wth: IWeather[]) {
        this.wthers.next(wth);
    }
    getWthers() {
        return this.wthers.asObservable();
    }
    // search(title: String) {
    //     return this.getWeathers().pipe(map((weathers) => {
    //         return weathers.filter((weather) => {
    //             return weather.name.toLowerCase().includes(title.toLowerCase());
    //         });
    //     }));
    // }

    findByCityName(cityName: string): Observable<IWeather[]> {
        return this.http.get<IWeather[]>(`${this.Port}:5000/api/weather/findbycityname/${cityName}`);
    }

}
