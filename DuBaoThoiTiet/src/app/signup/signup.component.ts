import { Component, OnInit } from '@angular/core';
import { User } from '../interfaces/user.interface';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  checked = false;

  constructor(private userService: UserService, private router: Router, private toastService: ToastrService) { }

  ngOnInit() {
  }

  onChange(e) {
    this.checked = !this.checked;
  }
  isHome() {
    this.router.navigate([`/home`]);
  }
  onSignUp(fullname, email, password){
    const user: User= {
      fullName: fullname,
      email: email,
      hash_password: password
    };
    this.userService.onSignUp(user).subscribe(res => {
      if(res){
        alert("Đăng ký thành công");
        this.router.navigate(['login']);
      }
    },err => {
      if(err.status === 404){
        console.log(err.error.message);
        alert(err.error.message);
      }
    })
  }
}
