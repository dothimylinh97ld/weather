var express = require("express");
//khởi tạo server
const bodyParser = require("body-parser");
var app = express();
var weather = require("./weather");
const PORT = process.env.PORT || 5000; // process.env là 1 object chưa tất cả các thông tin về môi trường mà nodejs đang chạy

app.use(bodyParser.json());

app.listen(PORT, function() {
  console.log("Listening on " + PORT);
});

app.get("/", (req, res) => {
  res.send("listening on " + PORT);
});

app.get("/weather", (req, res) => {
  weather.find().then(
    weather => {
      res.send({ weather });
    },
    e => {
      res.status(400).send(e);
    });
});