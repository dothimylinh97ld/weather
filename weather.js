const mongooes = require("mongoose");

var schema = mongooes.Schema;
const db = require("./db");

const weatherSchema = mongooes.Schema({
  id:{
    type:Number,
    required: true, 
    unique: true
  },
  Date:{
      type: String,
      required: true,
      unique: true
  },
  day:{
    type: String, 
    required: true, 
    unique: true
  },
  country: {
      type: String,
      required: true,
      unique: true,
  },
  city:{
    type: String,  
    required: true, 
    unique: true
  },
  status:{
    type: String,  
    required: true, 
    unique: true

  },
  temperature:{
    type: String,  
    required: true, 
    unique: true
  }
});

const weather = mongooes.model("weather", weatherSchema); //biên dịch mô hình cho schema.
// tham số thứ 1 là để tao ra 1 colecction cho mô hình, tham số thứ 2 là là schema mà muốn dùng để tạo mô hình

module.exports = weather;